# -*- encoding: utf-8 -*-
$:.unshift File.expand_path("../lib", __FILE__)
require "tado/version"

Gem::Specification.new do |s|
  s.name          = "ruby-tado"
  s.version       = Tado::VERSION
  s.authors       = [ "Stephane D'Alu" ]
  s.email         = [ "sdalu@sdalu.com" ]
  s.homepage      = "http://gitlab.com/sdalu/ruby-tado"
  s.summary       = "Access to Tado API"

  s.add_dependency 'faraday'
  s.add_dependency 'oauth2'
  s.add_dependency 'faraday_middleware-oauth2_refresh'
  s.add_dependency 'tzinfo'
  s.add_dependency 'tzinfo-data'

  
  s.add_development_dependency "yard"
  s.add_development_dependency "rake"
  s.add_development_dependency "redcarpet"

  s.license       = 'MIT'

  s.files         = %w[ LICENSE Gemfile ruby-tado.gemspec ] + 
		     Dir['lib/**/*.rb'] 
end
