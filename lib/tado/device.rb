class Tado

#
# Class wrapping interaction with the Tado Device
#
class Device    
    # Create a device instance associated to the zone from
    # the JSON returned by the Tado API
    #
    # @param json   [Object] json returned by the Tado API
    # @param zone   [Zone  ] zone to which this device belongs
    def self.from_json(json, zone:)
        self.new(json.dig('serialNo'),
                 data: self.parse_from_json(json),
                 zone: zone)
    end


    # Serial number of device
    #
    # @return [String] serial number
    attr_reader :serial

    
    # Type of device (heating, airconditioning)
    #
    # @return [:HEATING] heating device
    # @return [Symbol]
    attr_reader :type

    
    # Current firmware version
    #
    # @return [String] firmware version
    attr_reader :firmware

    
    # Duties performed by the device
    #
    # @return [Array<Symbol>] list of duties
    #   * [:ZONE_UI    ] user interface
    #   * [:ZONE_DRIVER] drive the heating/aircon
    #   * [:ZONE_LEADER] used as reference for temperature
    attr_reader :duties

    
    # Battery status
    #
    # @return [:NORMAL]
    attr_reader :battery

    
    # Capabilities
    #
    # @return [Array<Symbol>] list of capabilities
    #   * [:INSIDE_TEMPERATURE_MEASUREMENT] temperature
    #   * [:IDENTIFY                      ] identify
    attr_reader :capabilities

    
    # Time of device calilbration
    #
    # @return [nil ]  device was not calibrated
    # @return [Time]  timestamp of device calibration
    attr_reader :calibrated

    
    # Time of last connection
    #
    # @return [nil ]  device was not connected
    # @return [Time]  timestamp of last device connection  
    attr_reader :connection

    
    # Create a device instance associated to the zone
    #
    # @param serial [String] device serial number
    # @param zone   [Zone  ] zone to which this device belongs
    # @param data   [Hash{Symbol => Object}] initialising data
    def initialize(serial, data: nil, zone:)
        @zone   = zone
        @serial = serial

        data&.each {|k, v| instance_variable_set("@#{k}", v) }
    end

    private

    def self.parse_from_json(json)
        {
            :type         => json.dig('deviceType'),
            :firmware     => json.dig('currentFwVersion'),
            :duties       => json.dig('duties').map(&:to_sym),
            :battery      => json.dig('batteryState').to_sym,
            :capabilities => json.dig('characteristics', 'capabilities')
                                 .map(&:to_sym),
            :calibrated   => if json.dig('mountingState', 'value') == 'CALIBRATED' 
                                 Time.parse(json.dig('mountingState', 'timestamp'))
                             end,
            :connection   => if json.dig('connectionState', 'value')
                                 Time.parse(json.dig('connectionState', 'timestamp'))
                             end,
        }
    end    
    
end
end
