require 'time'
require 'tzinfo'

class Tado

#
# Class wrapping interaction with the Tado Home
#
class Home
    # Create a new Home associated to the Tado account
    #
    # @param id   [Integer]
    # @param tado [Tado]
    def initialize(id, tado:)
        @tado = tado
        @id   = id

        refresh!
    end


    # Retrieve weather information associated with this home
    #
    # @return [Hash<Symbol,Object>] weather information
    #   * :solar_intensity  [Float]
    #   * :temperature      [Float]
    #   * :state            [Symbol]
    #   * :timestamp        [Time]
    def getWeather
        w  = v2_get('weather')
        
        ts = [ 'solarIntensity', 'outsideTemperature', 'weatherState' ]
                 .map {|k| w.dig(k, 'timestamp') }.uniq
        raise ParsingError if ts.size != 1

        {   :solar_intensity => w.dig('solarIntensity',     'percentage'),
            :temperature     => w.dig('outsideTemperature', 'celsius'),
            :state           => w.dig('weatherState',       'value').to_sym,
            :timestamp       => Time.parse(ts.first)
        }
    end

    
    # Retrieve zones associated with this home
    #
    # @return [Array<Zone>] list of zones
    def getZones
        v2_get('zones').map{|data|
            Zone.from_json(data, home: self)
        }
    end

    
    # Home identifier
    #
    # @return [Integer] identifier
    attr_reader :id

    
    # Timezone
    #
    # @return [TZInfo::Timezone] timezone
    attr_reader :tz

    
    # Creation time
    #
    # @return [Time]  timestamp
    attr_reader :created

    
    # Name of home
    #
    # @return [String] name
    attr_reader :name

    
    # Geolocalisation (GPS coordinate)
    #
    # @return [Array<Numeric>] latitude/longitude
    attr_reader :geoloc

    
    # Distance to consider user away from home
    #
    # @return [Float] distance in meters
    attr_reader :away_radius

    
    # Home address
    #
    # @return [Hash{Symbol => String,Array<String>}] home address
    #   * :address [Array<String>]	address
    #   * :zipcode [String]		zip code
    #   * :city    [String]		city
    #   * :state   [String]		state
    #   * :country [String]		country (iso3)
    attr_reader :address

    
    # Home contact
    #
    # @return [Hash{Symbol => String}] contact information
    #   * :name    [String]		contact name
    #   * :email   [String]		email address
    #   * :phone   [String]		phone number
    attr_reader :contact

    
    # Force a refresh of home attributs
    #
    # @return [self]
    def refresh!
        data = @tado.v2_get("homes/#{@id}")
        @tz          = TZInfo::Timezone.get(data.dig('dateTimeZone'))
        @created     = Time.parse(data.dig('dateCreated'))
        @name        = data.dig('name')
        @geoloc      = [ data.dig('geolocation', 'latitude'),
                         data.dig('geolocation', 'longitude') ]
        @away_radius = data.dig('awayRadiusInMeters')
        @address     = {
            :address => [ data.dig('address', 'addressLine1'),
                          data.dig('address', 'addressLine2') ].compact,
            :zipcode => data.dig('address', 'zipCode'),
            :city    => data.dig('address', 'city'),
            :state   => data.dig('address', 'state'),
            :country => data.dig('address', 'country')
        }.compact
        @contact     = data.dig('contactDetails').transform_keys(&:to_sym)
        self
    end

    
    # Retrieve a resource as a JSON
    # 
    # @param [String] resource relative to the home
    # @return a JSON structure
    def v2_get(resource, **opts)
        @tado.v2_get("homes/#{@id}/#{resource}", **opts)
    end    
end
end
