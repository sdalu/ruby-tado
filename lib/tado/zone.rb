class Tado
#
# Class wrapping interaction with the Tado Zone
#
class Zone
    # Create a zone instance associated to the home from
    # the JSON returned by the Tado API
    #
    # @param json   [Object] json returned by the Tado API
    # @param home   [Home  ] home to which this zone belongs
    def self.from_json(json, home:)
        self.new(json.dig('id'),
                 data: self.parse_from_json(json),
                 home: home)
    end

    # Parent home
    #
    # @return [Home] home
    attr_reader :home
    
    # Zone identifier
    #
    # @return [Integer] identifier
    attr_reader :id

    
    # Name of zone
    #
    # @return [String] name
    attr_reader :name


    # Type of zone (heating, airconditioning)
    #
    # @return [:HEATING] heating zone
    # @return [Symbol]
    attr_reader :type

    
    # Devices associated with this zone
    #
    # @return [Array<Device>] list of devices
    attr_reader :devices


    # Dazze mode
    #
    # @return [nil] not supported
    # @return [Boolean] is dazzle mode enabled
    attr_reader :dazzle

    
    # Open window detection
    #
    # @return [nil] not supported
    # @return [Boolean] is window detection enabled
    attr_reader :open_window

    
    # Open window timeout
    #
    # @return [nil] not supported
    # @return [Integer] is window detection enabled
    attr_reader :open_window_timeout


    # Create a zone instance associated to the home
    #
    # @param id     [String] home identifier
    # @param home   [Home  ] home to which this zone belongs
    # @param data   [Hash{Symbol => Object}] initialising data
    def initialize(id, data:, home:)
        @home = home
        @id   = id

        data&.each {|k, v| instance_variable_set("@#{k}", v) }
    end


    # Retrieve history (temperature, humidity, ...) for this zone
    #
    # @param  date [String] date in yyyy-mm-dd format
    # @return [Hash{Symbol => Object}] history of the given day.
    # For example:
    #    { :wheather => {
    #        :condition => [ [ timeFrom..timeTo, state, temperature ]... ],
    #        :sunny     => [ [ timeFrom..timeTo, is_sunny ]... ],
    #        :slots     => { 'hh:mm' => [ state, temperature ] } },
    #      :settings => [ [ timeFrom..timeTo, type, powered, temperature ]... ],
    #      :measure  => {
    #        :temperature      => [ [ time, temperature ]... ],
    #        :humidity         => [ [ time, humidity ]... ],
    #        :device_connected => [ [ timeFrom..timeTo, is_connected ]... ] },
    #      :stripes => [ [ timeFrom..timeTo, mode, type, is_powerd, temperature ]... ]
    #    }
    # 
    def getHistory(date = Date.today)
        History.get(date, zone: self)
    end

    
    # Retrieve a resource as a JSON
    # 
    # @param [String] resource relative to the zone
    # @return a JSON structure
    def v2_get(resource, **opts)
        @home.v2_get("zones/#{@id}/#{resource}", **opts)
    end        


    private

    def self.parse_from_json(json)
        supported_enabled = ->(d) {
            d.dig('supported') ? d.dig('enabled') : nil }
        {
            :name        => json.dig('name'),
            :type        => json.dig('type'),
            :created     => Time.parse(json.dig('dateCreated')),
            :devices     => json.dig('devices').map {|data|
                               Device.from_json(data, zone: self) },
            :dazzle      => supported_enabled.(json.dig('dazzleMode')),
            :open_window => supported_enabled.(json.dig('openWindowDetection')),
            :open_window_timeout => json.dig('openWindowDetection', 'timeoutInSeconds'),
        }
    end
    
end
end
