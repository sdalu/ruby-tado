class Tado


# Standard Tado error
class Error < StandardError
end

# Error related to the parsing/processing of Tado data
class ParsingError < Error
end

# Network Error
class NetError < Error
end

# Resource not found
class NotFound < NetError
end

end
