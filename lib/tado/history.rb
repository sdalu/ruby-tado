class Tado
module History
    def self.get(date = Date.today, home: nil, zone: nil, tado: nil)
        date = case date
               when Date                  then date.strftime('%Y-%m-%d')
               when /^\d{4}-\d{2}-\d{2}$/ then date
               else raise ArgumentError
               end

        report = if Zone === zone
                     zone.v2_get("dayReport", date: date)
                 elsif Tado === tado && Integer === home && Integer === zone
                     tado.v2_get("homes/#{home}/zones/#{zone}/dayReport", date: date)
                 else
                     raise ArgumentError
                 end
        self.parse_from_json(report)
    end

    
    private    
    
    MAPPING = [ {
              name: [ :weather, :condition ],
            series: [ 'weather', 'condition' ],
            fields: [ 'state',
                      [ 'temperature', 'celsius' ] ] },
          {   name: [ :weather, :sunny ],
            series: [ 'weather', 'sunny' ] },
          {   name: [ :weather, :slots ],
            series: [ 'weather', 'slots' ],
            fields: [ 'state', [ 'temperature', 'celsius' ] ] },
          {   name: :settings,
            series: [ 'settings' ],
            fields: [ 'type', 'power',
                      [ 'temperature', 'celsius' ] ] },
          {   name: [ :measured, :temperature ],
            series: [ 'measuredData', 'insideTemperature' ],
            fields: [ 'celsius' ] },
          {   name: [ :measured, :humidity ],
            series: [ 'measuredData', 'humidity' ] },
          {   name: [ :measured, :device_connected ],
            series: [ 'measuredData', 'measuringDeviceConnected' ] },
          {   name: :stripes,
            series: [ 'stripes' ],
            fields: [ 'stripeType',
                      [ 'setting', 'type' ],
                      [ 'setting', 'power' ],
                      [ 'setting', 'temperature', 'celsius' ] ] },
          {   name: :heating,
            series: [ 'callForHeat' ] }
        ]

    
    def self.parse_from_json(json)
        h = {}
        MAPPING.each {|name:, series:, fields: nil|
            e      = h
            fields = [ nil ] if fields.nil? || fields.empty?
            name    = Array(name)
            name[0..-2].each {|n| e = e[n] ||= {} }
            e[name.last] = History.timeSeries(json.dig(*series), fields)
        }
        h
    end


    def self.timeSeries(json, template)
        case type = json.dig('timeSeriesType')
        when 'slots'
            json.dig('slots').transform_values {|v|
                template.map {|fields| v.dig(*fields) }
            }

        when 'dataIntervals'
            json.dig('dataIntervals').map {|v|
                [ Time.at(Time.parse(v.dig('from')).to_i, in: 0) ..
                  Time.at(Time.parse(v.dig('to'  )).to_i, in: 0) ] +
                    template.map {|fields| v.dig('value', *fields) }
            }

        when 'dataPoints'
            json.dig('dataPoints').map {|v|
                [ Time.at(Time.parse(v.dig('timestamp')).to_i, in: 0) ] +
                    template.map {|fields| v.dig('value', *fields) }
            }

        else raise ParsingError
        end
    end
        
end
end
