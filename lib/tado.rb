require 'json'
require 'date'
require 'faraday'
require 'oauth2'
require 'faraday_middleware/oauth2_refresh'



#
# Access to the Tado API
#
# Unoffical API documentation is provided by:
# * https://shkspr.mobi/blog/2019/02/tado-api-guide-updated-for-2019/
# * http://blog.scphillips.com/posts/2017/01/the-tado-api-v2/
#
# Web app client id/secret are retrieved from:
# * https://my.tado.com/webapp/env.js
#
# Example:
#
#    require 'tado'
#
#    $t = Tado.new('login', 'password')
#    puts $t.getHomes.first.getZones.first.getHistory.inspect
#
class Tado
    # Client ID used the the Tado web application
    CLIENT_ID     = 'tado-web-app'
    # Client secret used by the Tado web application
    CLIENT_SECRET = 'wZaRN7rpjn3FoNyF5IFuxg9uMzYJcvOo' \
                    'Q8QWiIqS3hfk6gLhVlG57j5YNoZL2Rtc'
    # Site used sefor authentication
    AUTH_SITE     = 'https://auth.tado.com'.freeze
    # Site used for API requests
    API_SITE      = 'https://my.tado.com'.freeze

    
    # Name of Tado owner
    #
    # @return [String]
    attr_reader :name

    
    # Email of Tado owner
    #
    # @return [String]
    attr_reader :email

    
    # Username of Tado owner
    #
    # @return [String]
    attr_reader :username

    
    # Create a new instance of Tado
    #
    # @param [String] username account username
    # @param [String] password account password
    # @param [String] client_id     client id used for oauth2
    # @param [String] client_secret client secret used for oauth2
    def initialize(username, password,
                   client_id: CLIENT_ID, client_secret: CLIENT_SECRET)
        tclient = OAuth2::Client.new(client_id, client_secret, site: AUTH_SITE)
        @token  = tclient.password.get_token(username, password)

        @client = Faraday.new(url: API_SITE) do |f|
            f.request  :oauth2_refresh, @token
            f.request  :url_encoded
            f.adapter  :net_http
        end

        refresh!
    end

    
    # Retrieve list of homes associated with the tado account
    #
    # @return [Array<Home>] list of homes
    def getHomes
        v2_get('me').dig('homes').map {|h|
            Home.new( h.dig('id'), tado: self )
        }
    end

    
    # Shortcut to get all zones
    #
    # @return [Array<Zone>] list of zone
    def getAllZones
        getHomes.flat_map {|h| h.getZones }
    end
    
    
    # Shortcut to get zone history from home/zone identifiers.
    # @see Zone#getHistory
    #
    # @param home [Integer] home identifier
    # @param zone [Integer] zone identifier
    # @return [Hash{Symbol => Object}] history of the given day.
    def getHistory(date = Date.today, home:, zone:)
        History.get(date, home: home, zone: zone, tado: self)
    end

    
    # Force a refresh of tado attributs
    #
    # @return [self]
    def refresh!
        data = v2_get('me')
        @name     = data.dig('name')
        @email    = data.dig('email')
        @username = data.dig('username')
        self
    end

    
    # Retrieve a resource as a JSON
    # 
    # @param [String] resource relative to the v2 API
    # @return a JSON structure
    # @raise [NotFound] the resource was not found
    # @raise [NetError] a network error occured
    def v2_get(resource, **opts)
        resp = @client.get("/api/v2/#{resource}", **opts)
        case resp.status
        when 200
        when 404 then raise NotFound
        else          raise NetError
        end

        JSON.parse(resp.body)
    end    

    def inspect
        "#<#{self.class} #{@username}>"
    end

    alias_method :to_s, :inspect
        
end

require_relative 'tado/version'
require_relative 'tado/errors'
require_relative 'tado/home'
require_relative 'tado/zone'
require_relative 'tado/device'
require_relative 'tado/history'
